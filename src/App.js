import "./App.css";
import io from "socket.io-client";
import { useState } from "react";

import Chat from "./Chat.js";

const socket = io.connect(`${process.env.REACT_APP_API_URL}`);

function App() {
  const [username, setUsername] = useState("");
  const [room, setRoom] = useState("");
  const [showChat, setShowChat] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  socket.on("connect", () => {
    setIsLoading(false);
  });

  const joinRoom = () => {
    if (username !== "" && room !== "") {
      socket.emit("join_room", room);
      setShowChat(true);
    }
  };

  return (
    <>
      {isLoading ? (
        <div className="loader">
          <div className="square" id="sq1"></div>
          <div className="square" id="sq2"></div>
          <div className="square" id="sq3"></div>
          <div className="square" id="sq4"></div>
          <div className="square" id="sq5"></div>
          <div className="square" id="sq6"></div>
          <div className="square" id="sq7"></div>
          <div className="square" id="sq8"></div>
          <div className="square" id="sq9"></div>
        </div>
      ) : (
        <div className="App">
          {!showChat ? (
            <div className="joinChatContainer">
              <h3>Chat Me Not</h3>
              <input
                type="text"
                placeholder="Enter Username"
                onChange={(event) => {
                  setUsername(event.target.value);
                }}
              ></input>
              <input
                type="text"
                placeholder="Enter Room ID."
                onChange={(event) => {
                  setRoom(event.target.value);
                }}
              ></input>
              <button onClick={joinRoom}>Join A Room</button>
            </div>
          ) : (
            <Chat socket={socket} username={username} room={room} />
          )}
        </div>
      )}
    </>
  );
}

export default App;
